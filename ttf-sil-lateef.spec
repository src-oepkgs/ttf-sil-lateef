Name: ttf-sil-lateef
Version: 1.200
Release: 7
Summary: 'An Arabic script font for Sindhi and other languages of southern Asia'
License: OFL
Conflicts: ttf-sil-fonts
#Source0: http://software.sil.org/downloads/r/lateef/LateefGR-1.200.zip
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%global _fname LateefGR
%define pkgname ttf-sil-lateef
%define _pkgname lateef

%prep
 

%description
'An Arabic script font for Sindhi and other languages of southern Asia'

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    cd "%{_fname}-$pkgver";
    find -type f -name "%{_fname}*.ttf" -execdir install -Dm644 -t "$pkgdir/usr/share/fonts/TTF" {} \;;
    install -Dm644 -t "$pkgdir/usr/share/doc/$pkgname" README.txt FONTLOG.txt documentation/*.pdf;
    install -Dm644 -t "$pkgdir/usr/share/licenses/$pkgname" OFL.txt

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

